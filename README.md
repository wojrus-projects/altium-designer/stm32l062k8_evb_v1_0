# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU STM32L062K8T6 (https://www.st.com/en/microcontrollers-microprocessors/stm32l062k8.html).

# Projekt PCB

Schemat: [doc/STM32L062K8_EVB_V1_0_SCH.pdf](doc/STM32L062K8_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/STM32L062K8_EVB_V1_0_3D.pdf](doc/STM32L062K8_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
